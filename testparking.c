/* 
 * file: testcoop.c 
 */
#include <8051.h>
#include "preemptive.h"

__data __at(0x34) char active_thread_map;
__data __at(0x39) Semaphore sem_main;

// Globals related to parking lot
__data __at(0x3a) char slots[2];
__data __at(0x3c) Semaphore sem_sync;
__data __at(0x3d) Semaphore sem_park;

// Semaphore for printing results via UART
__data __at(0x7a) Semaphore sem_print;
__data __at(0x7b) char dels[4];

// Time characters, higher 4 bits & lower 4 bits,
//   indexed by TID
__data __at(0x4c) char ht[4];
__data __at(0x5c) char lt[4];

// Slots that each thread acquired, resp.
__data __at(0x6c) char got_park[4];

// The current_thread
__data __at(0x35) char current_thread;

// The thread map
__data __at (0x34) char active_thread_map;

#define PrintRandNum(num) \
    ht[current_thread] = num >> 4; \
    lt[current_thread] = num & 0x0f; \
    if (ht[current_thread] <= 9) { \
        ht[current_thread] = ht[current_thread] + '0'; \
    } else { \
        ht[current_thread] = ht[current_thread] + 'a' - 10; \
    } \
    if (lt[current_thread] <= 9) { \
        lt[current_thread] = lt[current_thread] + '0'; \
    } else { \
        lt[current_thread] = lt[current_thread] + 'a' - 10; \
    } \
    SBUF = 'r'; \
    while (!TI); \
    TI = 0; \
    SBUF = ht[current_thread]; \
    while (!TI); \
    TI = 0; \
    SBUF = lt[current_thread]; \
    while (!TI); \
    TI = 0; \

// Output log via the serial port
#define PrintToUART(status) \
    /* Get higher and lower 4 bits into the array */ \
    ht[current_thread] = now() >> 4; \
    lt[current_thread] = now() & 0x0f; \
    if (ht[current_thread] <= 9) { \
        ht[current_thread] = ht[current_thread] + '0'; \
    } else { \
        ht[current_thread] = ht[current_thread] + 'a' - 10; \
    } \
    if (lt[current_thread] <= 9) { \
        lt[current_thread] = lt[current_thread] + '0'; \
    } else { \
        lt[current_thread] = lt[current_thread] + 'a' - 10; \
    } \
    /* Sentence format: */ \
    /* He/She entered space #? at time ?? */ \
    /* Who */ \
    SBUF = current_thread + '0'; \
    while (!TI); \
    TI = 0; \
    /* "Enters" */ \
    SBUF = status; \
    while (!TI); \
    TI = 0; \
    /* Space number */ \
    SBUF = got_park[current_thread] + '0'; \
    while (!TI); \
    TI = 0; \
    /* Print time */ \
    SBUF = ht[current_thread]; \
    while (!TI); \
    TI = 0; \
    SBUF = lt[current_thread]; \
    while (!TI); \
    TI = 0; \
    /* Space */ \
    SBUF = ' '; \
    while (!TI); \
    TI = 0;

/* Pseudorandom generator */
__data __at(0x4b) char last_x;
__data __at(0x4a) char c;
__code char a = 97;
__code char b = 255;
#define INIT_RAND() \
    c = 37; \
    last_x = 2;
// problem: 59 (seems ok?) 118
// ok: 43 97 53

char random(void) {
    char x = last_x;
    last_x = (a * x + c) % b;
    c = (a * x + c) / b;
    return last_x;
}

/* Concat "Park" and number n to form a new
 * identifier "Parkn" */
#define FNAME(n) Park ## n

/* Macro to generate nth Park function */
#define PARK(n) \
void FNAME(n)(void) __using (n) { \
    SemaphoreWait(sem_park); \
    /** Enter the slot **************/ \
    SemaphoreWait(sem_sync); \
    if (slots[0] == 0) { \
        slots[0] = 1; \
        got_park[current_thread] = 0; \
    } else if (slots[1] == 0) { \
        slots[1] = 1; \
        got_park[current_thread] = 1; \
    } \
    /* Print entered log */ \
    SemaphoreWait(sem_print); \
    dels[current_thread] = random() % 50; \
    PrintRandNum(dels[current_thread]); \
    PrintToUART('e'); \
    SemaphoreSignal(sem_print); \
    SemaphoreSignal(sem_sync); \
    /********************************/ \
    \
    /* Sleep for random time */ \
    delay(dels[current_thread]); \
    \
    /** Leave the slot **************/ \
    SemaphoreWait(sem_sync); \
    /* Print left log */ \
    SemaphoreWait(sem_print); \
    PrintToUART('l'); \
    SemaphoreSignal(sem_print); \
    \
    slots[got_park[current_thread]] = 0; \
    SemaphoreSignal(sem_sync); \
    /********************************/ \
    \
    SemaphoreSignal(sem_park); \
    SemaphoreSignal(sem_main); \
    ThreadExit(); \
}

/* Generate the three Park function */
PARK(1)
PARK(2)
PARK(3)

void main(void) {
    INIT_RAND();

    // Tx initialization
    TMOD |= 0x20;
    TH1 = -6;
    SCON = 0x50;
    TR1 = 1;

    // Initialize slots
    slots[0] = 0;
    slots[1] = 0;

    // Initialize semaphores
    SemaphoreCreate(sem_print, 1);
    SemaphoreCreate(sem_park, 2); // Two parking slots
    SemaphoreCreate(sem_sync, 1); // Mutex to sync slots
    SemaphoreCreate(sem_main, 3); // Prevent it from creating >3 threads

    // Aqcuire lock before creating threads
    SemaphoreWait(sem_main);
    ThreadCreate((FunctionPtr) Park1);

    SemaphoreWait(sem_main);
    ThreadCreate((FunctionPtr) Park2);

    SemaphoreWait(sem_main);
    ThreadCreate((FunctionPtr) Park3);

    SemaphoreWait(sem_main);
    // Pick the corresponding Park$ function
    //   according to the thread bitmap
    while (1) {
        EA = 0;
        if (!(active_thread_map & 0x02)) {
            ThreadCreate((FunctionPtr) Park1);
            break;
        }
        else if (!(active_thread_map & 0x04)) {
            ThreadCreate((FunctionPtr) Park2);
            break;
        }
        else if (!(active_thread_map & 0x08)) {
            ThreadCreate((FunctionPtr) Park3);
            break;
        }
        EA = 1;
    }

    SemaphoreWait(sem_main);
    // Pick the corresponding Park$ function
    //   according to the thread bitmap
    while (1) {
        EA = 0;
        if (!(active_thread_map & 0x02)) {
            ThreadCreate((FunctionPtr) Park1);
            break;
        }
        else if (!(active_thread_map & 0x04)) {
            ThreadCreate((FunctionPtr) Park2);
            break;
        }
        else if (!(active_thread_map & 0x08)) {
            ThreadCreate((FunctionPtr) Park3);
            break;
        }
        EA = 1;
    }

    while(1);
}

// Jump to void Bootstrap(void) upon power on
void _sdcc_gsinit_startup(void) {
    __asm
        ljmp  _Bootstrap
    __endasm;
}

// Stub function to override the defaults generated
// by the compiler
void _mcs51_genRAMCLEAR(void) {}
void _mcs51_genXINIT(void) {}
void _mcs51_genXRAMCLEAR(void) {}

// ISR for timer 0. Jumps immediately to the real handler code.
void timer0_ISR(void) __interrupt(1) {
    __asm
        ljmp _myTimer0Handler
    __endasm;
}
