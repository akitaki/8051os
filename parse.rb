#!/usr/bin/ruby
class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end
end

def parse(str)
  slots = ['_', '_']
  entry_time = [nil, nil]
  expect_time = [nil] * 4

  arr = str.split.map do |line|
    if line.length > 5
      randstr = line[0..2]
      norm = line[3..7]
    else
      randstr = ''
      norm = line[0..4]
    end

    thread = norm[0].to_i
    verb = norm[1] == 'e' ? 'entered'.red : 'left   '.green
    slot = norm[2].to_i
    time = norm[3..4].to_i(16)

    if randstr.length > 0
      expect_time[thread] = randstr[1..2].to_i(16)
    end

    slots[norm[2].to_i] = if norm[1] == 'e'
                            entry_time[norm[2].to_i] = norm[3..4].to_i(16)
                            norm[0]
                          else
                            '_'
                          end

    print "Thread %d %s slot %d at time %3d   [%s, %s]" % [
      thread, verb, slot, time,
      slots[0], slots[1],
    ]

    get_rand_str = -> {
      "  Rand: #{randstr[1..2].to_i(16)}"
    }

    get_actu_str = -> { 
      str = "  Actu: %-3d (expected #{expect_time[thread]})" % [(time - entry_time[slot]) % 256]
      if ((time - entry_time[slot]) % 256).between?(expect_time[thread], expect_time[thread] * 1.5)
        str.green
      else
        str.red
      end
    }

    print "%s\n" % [
      randstr.length > 0 ? get_rand_str.call : get_actu_str.call
    ]
  end
end

def init
  exit if ARGV.length < 0
  str = ARGV[0]
  parse(str)
end

init
