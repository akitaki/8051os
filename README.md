# 8051 OS!

---

# 1. `delay()` and `now()`

## On the choice of TQ

From the project checkpoint manual it reads:

> An important consideration is that delay() is not an exact delay but is a delay for “at least n time units” and “less than (n + 0.5) time units” for it to be acceptable (otherwise, it rounds up to n+1 time units, which would not be correct).  Of course, the more accurate the better, but there is an inherent limit on how accurate it can be. 
> Based on the above requirement, state your choice of time unit and provide your justification for how you think you can implement a delay() that meets the requirement above.

Here I propose a possible way to choose the time quantum for `delay()`.

### Background Information

Assuming that we're using 13-bit timer to set up interrupts, then

- Every 8192 clock cycles, the timer generates an interrupt.
- Each clock cycle is approimately $`1\mu s`$

Hence, we have that interrupts (that we use to invoke the scheduler) are generated every $`0.008192s`$.

### Derivation of the Time Quantum

Let the interval between interrupts be $`\epsilon (= 0.008192s)`$.

Let the time quantum be denoted $`T = n\epsilon`$, where $`n`$ is a positive integer.

Imagine that we're in the worst possible case, where four threads $`t_0, \dots, t_3`$ that are `delay()`ed by $`aT, \dots, dT`$ respectively are to **wake at the same time**.

Choose the one that gets scheduled last, say $`t_3`$, then it must wait for additional time of $`3\epsilon`$ before running. On the other hand, the choice of $`T`$ must be made s.t. $`t_3`$ is run after waiting at most $`1.5dT`$. Thus,

```math
dT + 3\epsilon = (3 + nd)\epsilon < \frac 3 2 nd\epsilon = \frac 3 2 dT \\
\Leftrightarrow 6 < nd, \; \forall d\in\mathbb{N} \\
\Leftrightarrow 6 < n
```

Hence, for all $`T \geq 6\epsilon`$, we can guarantee the `delay()` function to be precise enough.

## Implementation detail

> what does your timer-0 ISR have to do to support these multiple delays and now()?

The timer handler keeps track of 

- A list of remaining delay values for `delay()`.
- A counter that keeps incrementing on every timer iterrupt.

> what if all threads call delay() and happen to finish their delays all at the same time?  How can you ensure the accuracy of your delay? (i.e., between n and n+0.5 time units)?  
> 
> How does the worst-case delay completion (i.e., all threads finish delaying at the same time) affect your choice of time unit?

This is already answered in the previous section where the lower bound of acceptable time quantum is derived.

# 2. Thread termination

`ThreadCreate()` and `ThreadExit()` are updated to use semaphores to prevent us from exceeding 4 threads:

```C
ThreadID ThreadCreate(FunctionPtr fp) {
    SemaphoreWait(sem_threads);
    // Create the thread...
}

void ThreadExit(void) {
    SemaphoreSignal(sem_threads);
    // Mark the thread as dead...
}
```

# 3. Parking Lot Example

Parsed output log example:

```BASH
$ ./parse.rb 'r643e001 r862e108 3l075 r8a3e078 2l17f r941e184 3l018 ra62e01e 1l133 2l0c9 '
Thread 3 entered slot 0 at time   1   [3, _]  Rand: 100
Thread 2 entered slot 1 at time   8   [3, 2]  Rand: 134
Thread 3 left    slot 0 at time 117   [_, 2]  Actu: 116 (expected 100)
Thread 3 entered slot 0 at time 120   [3, 2]  Rand: 138
Thread 2 left    slot 1 at time 127   [3, _]  Actu: 119 (expected 134)
Thread 1 entered slot 1 at time 132   [3, 1]  Rand: 148
Thread 3 left    slot 0 at time  24   [_, 1]  Actu: 160 (expected 138)
Thread 2 entered slot 0 at time  30   [2, 1]  Rand: 166
Thread 1 left    slot 1 at time  51   [2, _]  Actu: 175 (expected 148)
Thread 2 left    slot 0 at time 201   [_, _]  Actu: 171 (expected 166)
```

![Screenshot of the sample result](assets/shot-01.png)
